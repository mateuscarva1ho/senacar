﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SenaCar.View
{
    

    public class Veiculos
    {
        public string Nome_do_Modelo { get; set; }
        public float Preco_Modelo { get; set; }

        public string Nome_da_Marca { get; set; }

        public string PrecoVeiculos
        {
            get { return string.Format("R$ {0}", PrecoVeiculos); }
        }
    }


    public partial class ListagemView : ContentPage
    {
            public List<Veiculos> Veiculos { get; set; }
            

            public ListagemView()
            {
                InitializeComponent();

                this.Veiculos = new List<Veiculos>
                {
                       
                    new Veiculos {Nome_da_Marca = "Ferrari", Nome_do_Modelo = "Portofino", Preco_Modelo = 24000},
                    new Veiculos {Nome_da_Marca = "Bugatti", Nome_do_Modelo = "Veyron", Preco_Modelo = 32000},
                    new Veiculos {Nome_da_Marca = "Cruze", Nome_do_Modelo = "Hatch", Preco_Modelo = 44000},
                    new Veiculos {Nome_da_Marca = "Mercedes", Nome_do_Modelo = "C180", Preco_Modelo = 17000},
                    new Veiculos {Nome_da_Marca = "Land Rover", Nome_do_Modelo = "Evoque", Preco_Modelo = 234500}
                };
            
                this.BindingContext = this;

                
             

            }

        private void ListViewVeiculos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var veiculo = (Veiculos)e.Item;

            DisplayAlert("Serviço", string.Format("Voce Selecionou o serviço '{0}'. Valor: {1}", veiculo.Nome_da_Marca, veiculo.Preco_Modelo), "Ok");

            Navigation.PushAsync(new DescricaoView(veiculo));
        }
    }


    
         
}
