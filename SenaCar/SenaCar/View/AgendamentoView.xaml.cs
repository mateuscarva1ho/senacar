﻿using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using SenaCar.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SenaCar.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AgendamentoView : ContentPage
    {
        public AgendamentoView(Veiculos veiculos)
        {
            InitializeComponent();
            this.Title = veiculos.Nome_do_Modelo;
        }

        private void ButtonAgendamento_Clicked(object sender, EventArgs e)
        {
            Permissao();
        }
        public async void Permissao()
        {
            try
            {
                var status = await
                CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if (status != PermissionStatus.Granted)
                {
                    if (await

                    CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
                    {
                        await DisplayAlert("Permissão", "Precisamos da sua permissão para armazenar dados no dispositivo.", "OK");




                    }
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[]

                    { Permission.Storage });

                    status = results[Permission.Storage];
                }
                if (status == PermissionStatus.Granted)
                {
                    //await DisplayAlert("Sucesso!", "Prossiga com a operação", "OK");
                    FazerAgendamento();
                }
                else if (status != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Atenção", "Para utilizar os serviços de persistência de dados, aceite a permissão solicitada pelo dispositivo.", "OK");



                }
            }
            catch (Exception ex)
            {
                //await DisplayAlert("Erro", "Erro no processo " + ex.Message, "OK");
            }







        }

        public void FazerAgendamento()
        {
            using (var conexao = DependencyService.Get<ISQLite>().conexao())
            {
                AgendamentoDAO dao = new AgendamentoDAO(conexao);
                dao.Salvar(new Models.Agendamento("Freios", "5454515435", "mateus@email.com"));

                DisplayAlert("Sucesso!", "Agendamento foi realizando com sucesso", "Ok");
            }
        }
    }
}