﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SenaCar.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DescricaoView : ContentPage
    {
        private const float VIDROS_ELETRICOS = 545;
        private const float TRAVAS_ELETRICAS = 200;
        private const float AR_CONDICIONADO = 480;
        private const float CAMERA_RE = 160;
        private const float CAMBIO = 450;
        private const float SUSPENSAO = 300;
        private const float FREIOS = 245;

        public string TextoVidros
        {
            get
            {
                return string
                    .Format("Vidros Eletricos - R$ {0}", VIDROS_ELETRICOS);
            }
        }

        public string TextoTravas
        {
            get
            {
                return string
                    .Format("Travas Eletricas - R$ {0}", TRAVAS_ELETRICAS);
            }
        }
        public string TextoArCondicionado
        {
            get
            {
                return string
                    .Format("Ar Condicionado - R$ {0}", AR_CONDICIONADO);
            }
        }

        public string TextoCameraRe
        {
            get
            {
                return string
                    .Format("Camera de Re - R$ {0}", CAMERA_RE);
            }
        }

        public string TextoCambio
        {
            get
            {
                return string
                    .Format("Cambio - R$ {0}", CAMBIO);
            }
        }

        public string TextoSuspensao
        {
            get
            {
                return string
                    .Format("Suspensao - R$ {0}", SUSPENSAO);
            }
        }
        public string TextoFreios
        {
            get
            {
                return string
                    .Format("Freios - R$ {0}", FREIOS);
            }
        }

        public string ValorTotal
        {
            get
            {
                return string.Format("Valor Total: R$ {0}", veiculos.Preco_Modelo
               + (incluiVidrosEletricos ? VIDROS_ELETRICOS : 0)
                + (incluiTravasEletricas ? TRAVAS_ELETRICAS : 0)
                + (incluiArCondicionado ? AR_CONDICIONADO : 0)
               + (incluiCameraDeRe ? CAMERA_RE : 0)
                + (incluiCambio ? CAMBIO : 0)
                + (incluiSuspensao ? SUSPENSAO : 0)
                + (incluiFreios ? FREIOS : 0));
            }
        }

        //CAPTURANDO OPCIONAIS

        bool incluiVidrosEletricos;
        public bool IncluiVidrosEletricos
        {
            get
            {
                return incluiVidrosEletricos;
            }
            set
            {
                incluiVidrosEletricos = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiTransporte)
                //    DisplayAlert("VidrosEletricos", "Ativo", "OK");
                //else
                //    DisplayAlert("VidrosEletricos", "Inativo", "OK");
            }

        }

        bool incluiTravasEletricas;
        public bool IncluiTravasEletricas
        {
            get
            {
                return incluiTravasEletricas;
            }
            set
            {
                incluiTravasEletricas = value;
                OnPropertyChanged(nameof(ValorTotal));
                //    if (incluiBanhoRapido)
                //        DisplayAlert("TravasEletricas", "Ativo", "OK");
                //    else
                //        DisplayAlert("TravasEletricas", "Inativo", "OK");
                //}

            }
        }

        bool incluiArCondicionado;
        public bool IncluiArCondicionado
        {
            get
            {
                return incluiArCondicionado;
            }
            set
            {
                incluiArCondicionado = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiPasseio)
                //    DisplayAlert("ArCondicionado", "Ativo", "OK");
                //else
                //    DisplayAlert("ArCondicionado", "Inativo", "OK");
            }

        }

        bool incluiCameraDeRe;
        public bool IncluiCameraDeRe
        {
            get
            {
                return incluiCameraDeRe;
            }
            set
            {
                incluiCameraDeRe = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiMassagem)
                //    DisplayAlert("CameraDeRe", "Ativo", "OK");
                //else
                //    DisplayAlert("CameraDeRe", "Inativo", "OK");
            }

        }

        bool incluiCambio;
        public bool IncluiCambio
        {
            get
            {
                return incluiCambio;
            }
            set
            {
                incluiCambio = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiMassagem)
                //    DisplayAlert("Cambio", "Ativo", "OK");
                //else
                //    DisplayAlert("Cambio", "Inativo", "OK");
            }

        }

        bool incluiSuspensao;
        public bool IncluiSuspensao
        {
            get
            {
                return incluiSuspensao;
            }
            set
            {
                incluiSuspensao = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiMassagem)
                //    DisplayAlert("Suspensao", "Ativo", "OK");
                //else
                //    DisplayAlert("Suspensao", "Inativo", "OK");
            }

        }

        bool incluiFreios;
        public bool IncluiFreios
        {
            get
            {
                return incluiFreios;
            }
            set
            {
                incluiFreios = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiMassagem)
                //    DisplayAlert("Freios", "Ativo", "OK");
                //else
                //    DisplayAlert("Freios", "Inativo", "OK");
            }

        }
        public Veiculos veiculos { get; set; }
        public DescricaoView(Veiculos veiculos)
        {
            InitializeComponent();

            this.Title = veiculos.Nome_da_Marca;
            this.veiculos = veiculos;
            this.BindingContext = this;
        }

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AgendamentoView(veiculos));
            
        }
    }
}

