﻿using SenaCar;
using SenaCar.Data;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
[assembly: Xamarin.Forms.Dependency(typeof(SQLite_android))]
namespace SenaCar
{
    class SQLite_android : ISQLite
    {
        private const string arquivoDb = "Senacao.db3";

        public SQLiteConnection conexao()
        {
            var pathDb = Path.Combine(Android.OS.Environment.ExternalStorageDirectory.Path, arquivoDb);
            
            return new SQLite.SQLiteConnection(pathDb);
            //throw new NotImplementedException();

            
        }
    }
}
